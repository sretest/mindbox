import requests
from bs4 import BeautifulSoup
import time
from random import choice
import os

desktop_agents = ['Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
                 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
                 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
                 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/602.2.14 (KHTML, like Gecko) Version/10.0.1 Safari/602.2.14',
                 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
                 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
                 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
                 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
                 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
                 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0']

def random_headers():
    return {'User-Agent': choice(desktop_agents),'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'}


def send_telegram(text: str):
    token = os.getenv("TG_TOKEN")
    url = "https://api.telegram.org/bot"
    channel_id = os.getenv("TG_CHANNEL")
    url += token
    method = url + "/sendMessage"

    r = requests.post(method, data={
         "chat_id": channel_id,
         "text": text,
         "parse_mode":"HTML" 
	 })

    if r.status_code != 200:
        raise Exception("post_text error")

citlink='https://www.citilink.ru/catalog/noutbuki/LENOVO--noutbuki-lenovo-thinkpad/?sorting=price_asc'
lenovolink='https://shop.lenovo.ru/noutbuki/thinkpad/?sort=price&order=asc&max_price=70000'
knsnalichie='https://www.knskrsk.ru/multi/catalog/noutbuki/lenovo/thinkpad/_v-nalichii/_tsena-do-70000/'
knszakaz='https://www.knskrsk.ru/multi/catalog/noutbuki/lenovo/thinkpad/_pod-zakaz/_tsena-do-70000/'


def pars_kns(site: str):
    kns=''
    response = requests.get(site,headers=random_headers())
    if len(response.history) < 1:
        page = BeautifulSoup(response.content, 'lxml')
        item = page.find_all('div', class_='item col-12 col-sm-6 col-md-4 col-xl-4 border bg-white')
        if item:
            for i in item:
                if i.find('a', class_='name d-block'):
                    name = i.find('a', class_='name d-block').get('title')
                    link = i.find('a', class_='name d-block').get('href')
                else:
                    name=''
                    link=''
                if i.find('meta', itemprop='price'):
                    cost=i.find('meta', itemprop='price').get('content')
                else:
                    cost='NA' 
                kns+=name+"<a href='https://www.knskrsk.ru" + link + "'>  [" +cost+"]</a>\n"
        else:
            kns='NoInfo'
    else:
        kns='Empty because Redirect'        
    return kns


######### CITILINK #########
def pars_citilink(site: str):    
    citilink=''
    response = requests.get(site,headers=random_headers())
    page = BeautifulSoup(response.content, 'lxml')
    item = page.find_all('div', class_='ProductCardVertical_separated')
    if item:
        for i in item:
            if i.find('span', class_='js--ProductCardVerticalPrice__price-current_current-price'):
                cost = i.find('span', class_='js--ProductCardVerticalPrice__price-current_current-price').text.replace('\n','').replace(' ','')
            else:
                cost = 'NA'
            
            if i.find('a', class_='ProductCardVertical__name Link js--Link Link_type_default'):
                name = i.find('a', class_='ProductCardVertical__name Link js--Link Link_type_default').text.replace('\n','')
            else:
                name = 'oopS'
            if i.find('a', class_='ProductCardVertical__link'):
                link = i.find('a', class_='ProductCardVertical__link').get('href')    
            else:
                link=''    
            citilink+=(name + "<a href='https://www.citilink.ru"+link+"'> "+'[' + cost + '] </a>\n')
            
    else:
        citilink='NoInfo'
    return citilink
#########################################

def pars_lenovoshop(site: str):
    lenovo=''
    ########LENOVOSHOP###########
    response = requests.get(site,headers=random_headers())
    page = BeautifulSoup(response.content, 'lxml')
    item = page.find_all('div', class_='catalog-item') #'catalog-item__info')
    #print (item)
    for i in item:
        #name = i.find('div',class_='catalog-item__title').text
        name = i.find('input',class_='item_data').get('data-name')
        cost = i.find('input',class_='item_data').get('data-price')
        link = i.find('a' ,class_='catalog-item-go').get('href')
        lenovo+=name+"<a href='https://shop.lenovo.ru" + link + "'>  [" +cost+"]</a>\n"
    return lenovo



#############################
def main():
    while True:
        fnew = open('old.txt', 'r')
        filestring=fnew.read()
        fnew.close()
        parsestring='-----------\nCitilink:\n'+pars_citilink(citlink)+'-----------\nLenovoshop:\n'+pars_lenovoshop(lenovolink)+'-----------\nKNS в наличии:\n'+pars_kns(knsnalichie)+'\nKNS под заказ:\n'+pars_kns(knszakaz)+'-----------'

        if parsestring != filestring:
            send_telegram(parsestring)
            fnew = open ('old.txt', 'w')
            fnew.write(parsestring)
            fnew.close()
        time.sleep(600)

if __name__=="__main__": 
    main()
